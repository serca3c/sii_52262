#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#define BUFFER 55


int main( void ){

	int fd, error, lectura;

	char mensaje[BUFFER];

	//Crea el FIFO
	error = mkfifo( "/tmp/tuberia", 0600 );
	if ( error < 0 ) {
		perror( "No se puede crear el FIFO desde el logger");
		return( 0 );
	}

	//Abre el FIFO
	fd = open( "/tmp/tuberia", O_RDONLY );
	if ( fd < 0 ) {
		perror( "No puede abrirse el FIFO desde el logger" );
		return( 0 );
	}

	//Lee el FIFO e imprime el mensaje por pantalla
	error = 1;
	while ( error > 0 ) {
		lectura = read( fd, mensaje, sizeof( char ) * BUFFER );
		
		printf( "%s\n", mensaje );

		if ( lectura < 0 ) {
			perror( "Error de lectura del logger" );
			error = -1;
		}
		else if ( lectura == 0 ) {
			error = -1;
		}
	}

	//Cierra y elimina el FIFO
	close( fd );
	unlink("/tmp/tuberia");

	return( 0 );
}
