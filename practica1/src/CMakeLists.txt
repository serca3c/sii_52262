INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

SET(COMMON_SRCS 
	MundoServidor.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)
				
ADD_EXECUTABLE(servidor servidor.cpp ${COMMON_SRCS})

TARGET_LINK_LIBRARIES(servidor glut GL GLU pthread)


SET(COMMON_SRCS 
	MundoCliente.cpp
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)

ADD_EXECUTABLE(cliente cliente.cpp ${COMMON_SRCS})

TARGET_LINK_LIBRARIES(cliente glut GL GLU)



add_executable(logger logger.cpp)

add_executable(bot 
		bot.cpp
		Esfera.cpp
		Plano.cpp
		Raqueta.cpp
		Vector2D.cpp)

TARGET_LINK_LIBRARIES(bot glut GL GLU)
