#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main( void ){
	DatosMemCompartida datos, *pdatos;
	int fddatos;

	if ( ( fddatos = open( "/tmp/datos", O_RDWR, 0600 ) ) < 0 ) {
		perror( "Error al abrir el fichero para proyectar en memoria" );
		exit( 0 );
	}
	else {
		pdatos = ( DatosMemCompartida* ) mmap( NULL, sizeof( datos ), PROT_WRITE | PROT_READ, MAP_SHARED, fddatos, 0 );
		close( fddatos );
	}

	while ( pdatos->accion != 10 ){
		if ( pdatos->esfera.centro.y > ( pdatos->raqueta1.y2 + pdatos->raqueta1.y1 ) / 2 + 1 )
			pdatos->accion = 1;
		else if ( pdatos->esfera.centro.y < ( pdatos->raqueta1.y2 + pdatos->raqueta1.y1 ) / 2 - 1 )
			pdatos->accion = -1;
		else
			pdatos->accion = 0;

		usleep( 25000 );
	}

	munmap( pdatos, sizeof( DatosMemCompartida ) );

	return 0;
}
